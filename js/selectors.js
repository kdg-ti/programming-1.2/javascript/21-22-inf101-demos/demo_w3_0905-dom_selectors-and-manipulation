console.log("element with id firstDiv:",document.querySelector("#firstDiv").innerHTML);

console.log("inner HTML of first <p> tag:",document.querySelector("p").innerHTML);

console.log("Text content of all all <p> elements using enhanced for loop:");
for (const element of document.querySelectorAll("p")) {
  console.log("p >", element.textContent)
}

console.log("Text content of all all <p> elements using forEach:")
document.querySelectorAll("p")
  .forEach(element => console.log("p >", element.textContent))


console.log("innerHtml of nearest <body> parent of element with id secondDiv",
  document
  .getElementById("secondDiv")
  .closest("body")
  .innerHTML
)


console.log("innerHtml of direct parent of element with id secondDiv",document
  .getElementById("secondDiv")
  .parentNode
  .innerHTML
)

// append text at the end of secondDiv
const secondDiv = document.getElementById("secondDiv");
secondDiv.append("There's more\n");

// append button at the end of seconddiv
const button = document.createElement("button");
button.append(document.createTextNode("smaller <button"));
secondDiv.append(button);

// append button at the end of seconddiv using innerHTML produces bad HTML
//secondDiv.innerHTML += "<button> smaller &lt;button </button>"

// append button at the end of seconddiv using textContent produces correct HTML
const button2 = document.createElement("button");
button2.textContent="smaller <button";
secondDiv.append(button2);